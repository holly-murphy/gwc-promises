import "regenerator-runtime/runtime";
import fs from "fs";

// 1.
export const orderOfExecution = () => {
  console.log("first");
  console.log("second");
  console.log("third");
};

const mathHelper = (x) => x * 10;

// 2.
export const orderOfExecutionOfMath = () => {
  let x = 1;
  console.log(x);
  x = mathHelper(x);
  console.log(x);
  x = mathHelper(x);
  console.log(x);
};

// 3.
/* 
  setTimeout takes in a function to execute after specified timeout (ms)
*/
export const mathWithSetTimeout = () => {
  let x = 1;
  console.log(`1. `, x);
  x = 2;
  console.log(`2. `, x);
  setTimeout(() => {
    x = 3;
    console.log(`3. `, x);
  }, 1000);
  x = 4;
  console.log(`4. `, x);
};

// 4.
export const asynchronousEvents = () => {
  let data = "some-data";
  console.log(`1. : `, data);
  fs.readFile(`${__dirname}/text-example.txt`, "utf8", (err, readData) => {
    console.log(`2. `, readData);
  });
  console.log(`3. `, data);
};

const handlePromise = (x, err) => {
  if (err) {
    console.log(`promise err: `, err);
  } else {
    console.log(`promise resolved to: `, x);
  }
};

// 5. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
export const whatIsAPromise = () => {
  const myPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(`resolving to foo`);
      resolve("foo");
    }, 300);
  });

  myPromise
    .then((x) => handlePromise(x))
    .catch((err) => handlePromise(undefined, err));
};

// 6.
export const promiseRejection = () => {
  const myPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(`resolving to foo`);
      reject("foo");
    }, 300);
  });

  myPromise
    .then((x) => handlePromise(x))
    .catch((err) => handlePromise(undefined, err));
};

// 7. NOTE: both these calls return the same thing.
export const addSomeSyntacticSugar = async () => {
  const callbackResult = await callbackPlayground();
  console.log(`after callback function...`, callbackResult);

  const sugarResult = await syntacticSugar();
  console.log(`after syntacticSugar...`, sugarResult);
};

const callbackPlayground = () => {
  return new Promise((resolve, reject) => {
    fs.readFile("./text-example.txt", "utf8", (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  });
};

const syntacticSugar = async () =>
  fs.readFileSync("./text-example.txt", "utf8"); // await is implied
