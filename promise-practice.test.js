import {
  orderOfExecution,
  orderOfExecutionOfMath,
  mathWithSetTimeout,
  asynchronousEvents,
  whatIsAPromise,
  promiseRejection,
  addSomeSyntacticSugar,
} from "./promise-practice";

describe("promise-practice", () => {
  it("1. should console log...", () => {
    orderOfExecution();
  });

  it("2. should console log...", () => {
    orderOfExecutionOfMath();
  });

  it("3. should console log...", () => {
    mathWithSetTimeout();
  });

  it("4. should console log...", () => {
    asynchronousEvents();
  });

  it("5. should console log...", () => {
    whatIsAPromise();
    console.log(`in test after  calling whatIsAPromise()...`);
  });

  it("6. should console log...", () => {
    promiseRejection();
  });

  it.only("7. should console log...", async () => {
    await addSomeSyntacticSugar();
  });
});
